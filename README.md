# RadWatch Spectral Analysis

This is the repository for all analysis code and documentation related to spectroscopy for the RadWatch program.

The [program website](htttps://radwatch.berkeley.edu) has more general information about all of our activities.

We focus on spectral analysis of gamma sectra collected using HPGe, NaI, and CsI detectors. The code maintained in this repository is used to extract information about activity from background sources, and some common man-made sources seen as part of our overall background. We also apply these same methods for looking at heavy metal concentrations in samples using a neutron activation process. More information and tenchincal details can be found on our website.
